use crate::{error::Error, GenerationalIndex};
use std::{cell::RefCell, rc::Rc};

pub trait DisperseAlloc<T> {
    fn match_generation(&self, index: &GenerationalIndex) -> bool;
    fn allocate(&mut self, vec_len: usize) -> GenerationalIndex;
    fn deallocate(&mut self, index: &GenerationalIndex) -> Result<(), Error>;
}

pub struct DisperseVec<T, U>(Vec<T>, Rc<RefCell<U>>)
where
    U: DisperseAlloc<T>;

impl<T, U> DisperseVec<T, U>
where
    U: DisperseAlloc<T>,
{
    fn with_capacity(capacity: usize, alloc: U) -> DisperseVec<T, U> {
        DisperseVec(Vec::with_capacity(capacity), Rc::new(RefCell::new(alloc)))
    }
    fn get(&self, index: &GenerationalIndex) -> Option<&T> {
        if self.1.borrow().match_generation(index) {
            self.0.get(index.index())
        } else {
            None
        }
    }
    fn insert(&mut self, value: T) -> GenerationalIndex {
        let vector_length = self.0.len();
        let idx = self.1.borrow_mut().allocate(vector_length);
        self.0.insert(idx.index(), value);
        idx
    }
    fn remove(&mut self, index: GenerationalIndex) -> Result<(), Error> {
        if let Some(_val) = self.0.get(index.index()) {
            self.1.borrow_mut().deallocate(&index)?;
            self.0.remove(index.index());
            Ok(())
        } else {
            Err(Error::ElementNotFound)
        }
    }
}
