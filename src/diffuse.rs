use crate::Error;
use crate::GenerationalIndex;

pub trait DiffuseAllocator<T> {
    fn match_generation(&self, index: &GenerationalIndex) -> bool;
    fn allocate(&mut self, vec_len: usize) -> GenerationalIndex;
    fn deallocate(&mut self, index: &GenerationalIndex) -> Result<(), Error>;
}

pub struct DiffuseVec<'d, T, U>(Vec<T>, &'d mut U)
where
    U: DiffuseAllocator<T>;

impl<'d, T, U> DiffuseVec<'d, T, U>
where
    U: DiffuseAllocator<T>,
{
    fn with_capacity(capacity: usize, alloc: &'d mut U) -> DiffuseVec<'d, T, U> {
        DiffuseVec(Vec::with_capacity(capacity), alloc)
    }
    fn get(&self, index: &GenerationalIndex) -> Option<&T> {
        if self.1.match_generation(index) {
            self.0.get(index.index())
        } else {
            None
        }
    }
    fn insert(&mut self, value: T) -> GenerationalIndex {
        let vector_length = self.0.len();
        let idx = self.1.allocate(vector_length);
        self.0.insert(idx.index(), value);
        idx
    }
    fn remove(&mut self, index: GenerationalIndex) -> Result<(), Error> {
        if let Some(_val) = self.0.get(index.index()) {
            self.1.deallocate(&index)?;
            self.0.remove(index.index());
            Ok(())
        } else {
            Err(Error::ElementNotFound)
        }
    }
}
