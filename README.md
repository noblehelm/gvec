# gvec

**gvec** is a simple library for Generational Vecs and Indices written in Rust. It simply deals with insertion and removal of elements/values to the vector itself, while avoiding use-after-free scenarios and reusing available space.

## Current features

* Insertion
* Removal
* Automatic reallocation when the vector is full
* "vec![]"-like macro
* Iterator

## Possible new features

* Enumeration with support for GenerationalIndexing
