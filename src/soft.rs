use crate::{error::Error, index::GenerationalIndex};
use mockiato::mockable;

#[cfg_attr(test, mockable)]
pub trait SoftAlloc {
    fn match_generation(&self, index: &GenerationalIndex) -> bool;
    fn allocate(&mut self, vec_len: usize) -> GenerationalIndex;
    fn deallocate(&mut self, index: &GenerationalIndex) -> Result<(), Error>;
}

/// Soft version of a Vec with Generational scheme
///
/// Similar to the LightVec, the SoftVec is a data structure with a so-called
/// Generational scheme, that functions as a versioning scheme for indices. In
/// contrast to LightVec, there isn't an implemented allocator for the SoftVec.
/// Instead, there is a trait that *must* be implemented in order to provide
/// the Generational indices.
pub struct SoftVec<T>(Vec<T>);

impl<T> SoftVec<T> {
    /// Creates a new SoftVec
    ///
    /// Creates a new SoftVec, with the given capacity, in order to reduce
    /// initial allocations
    pub fn with_capacity(capacity: usize) -> Self {
        SoftVec(Vec::with_capacity(capacity))
    }

    /// Returns a value with the given index
    pub fn get(&self, index: &GenerationalIndex, alloc: &impl SoftAlloc) -> Option<&T> {
        if alloc.match_generation(index) {
            self.0.get(index.index())
        } else {
            None
        }
    }

    /// Inserts a value on the vector
    pub fn insert(&mut self, value: T, alloc: &mut impl SoftAlloc) -> GenerationalIndex {
        let temp_idx: GenerationalIndex = alloc.allocate(self.0.len());
        self.0.insert(temp_idx.index(), value);
        temp_idx
    }

    /// Removes a value with the given index
    pub fn remove(
        &mut self,
        index: GenerationalIndex,
        alloc: &mut impl SoftAlloc,
    ) -> Result<(), Error> {
        if let Some(_val) = self.0.get(index.index()) {
            alloc.deallocate(&index)?;
            self.0.remove(index.index());
            Ok(())
        } else {
            Err(Error::ElementNotFound)
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn test_insert() {
//         let mut salloc = SoftAllocMock::new();
//         let mock_index: GenerationalIndex = GenerationalIndex::new(0, 1);

//         salloc.expect_allocate(|arg| arg.any()).times(..).returns(GenerationalIndex::new(0, 1));
//         salloc.expect_match_generation(|arg| arg.partial_eq(&mock_index)).times(..).returns(true);

//         let mut muh_vec = SoftVec::with_capacity(3);
//         let idx = muh_vec.insert(3, &mut salloc);
//         assert_eq!(muh_vec.get(&idx, &salloc), Some(&3));
//     }
// }
