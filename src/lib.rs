mod dense;
mod diffuse;
mod disperse;
mod error;
mod index;
mod light;
mod soft;

pub use dense::DenseVec;
pub use error::Error;
pub use index::GenerationalIndex;
pub use light::LightVec;
pub use light::LightVecAllocator;
pub use soft::SoftAlloc;
pub use soft::SoftVec;

#[macro_export]
macro_rules! replace_expr {
    ($_t:tt $sub:expr) => {
        $sub
    };
}

#[macro_export]
macro_rules! dvec {
    ($($item:expr),*) => {
        {
            let quantity = { 0usize $(+ replace_expr!($item 1usize))* };
            let mut result_vec = DenseVec::with_capacity(quantity);
            let mut indices_vec: Vec<GenerationalIndex> = Vec::with_capacity(quantity);
            $(
                indices_vec.push(result_vec.insert($item));
            )*
            (result_vec, indices_vec)
        };
    };
    ($quantity:expr; $item:expr) => {
        {
            let mut result_vec = DenseVec::with_capacity($quantity);
            let mut indices_vec: Vec<GenerationalIndex> = Vec::with_capacity($quantity);
            for i in 0..$quantity {
                indices_vec.push(result_vec.insert($item));
            }
            (result_vec, indices_vec)
        };
    }
}

#[macro_export]
macro_rules! lvec {
    ($quantity:expr; $item:expr) => {{
        let (mut result_vec, mut vec_alloc) = LightVec::with_capacity($quantity);
        let mut indices_vec: Vec<GenerationalIndex> = Vec::with_capacity($quantity);
        for i in 0..$quantity {
            indices_vec.push(result_vec.insert($item, &mut vec_alloc));
        }
        (result_vec, vec_alloc, indices_vec)
    }};
}
