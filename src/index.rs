/// An index with a versioning scheme
///
/// The index for the GenerationalVec that has a so-called Generation scheme,
/// akin to a versioning system, that coupled with the GenerationalVec struct,
/// prevents invalid access to specific elements in the vector.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct GenerationalIndex {
    index: usize,
    generation: usize,
}

impl GenerationalIndex {
    pub fn new(index: usize, generation: usize) -> Self {
        GenerationalIndex { index, generation }
    }
    pub fn index(&self) -> usize {
        self.index
    }

    pub fn generation(&self) -> usize {
        self.generation
    }
}
