use gvec::Error;
use gvec::{lvec, GenerationalIndex, LightVec, LightVecAllocator};

#[test]
fn test_insertion() {
    let (mut test, mut test_alloc): (LightVec<u8>, LightVecAllocator) = LightVec::with_capacity(3);
    let test_idx = test.insert(1, &mut test_alloc);
    assert_eq!(test.get(&test_idx, &mut test_alloc), Some(&1));
}

#[test]
fn test_removal() {
    let (mut test, mut test_alloc): (LightVec<f32>, LightVecAllocator) = LightVec::with_capacity(3);
    let test_idx = test.insert(std::f32::consts::PI, &mut test_alloc);
    let test_idx_clone = test_idx.clone();
    assert_eq!(test.remove(test_idx, &mut test_alloc), Ok(()));
    assert_eq!(test.get(&test_idx_clone, &test_alloc), None);
}

#[test]
fn test_insertion_with_reallocation() {
    let (mut test, mut test_alloc, _) = lvec![3; 1];
    let fourth_idx = test.insert(4, &mut test_alloc);
    assert_eq!(test.get(&fourth_idx, &test_alloc), Some(&4));
}

#[test]
fn test_removal_with_wrong_generation() {
    let (mut test, mut test_alloc, test_idx) = lvec![1; std::f64::consts::PI];
    let new_idx = GenerationalIndex::new(test_idx[0].index(), test_idx[0].generation() + 1);
    assert_eq!(
        test.remove(new_idx, &mut test_alloc),
        Err(Error::WrongGeneration {
            expected: 0,
            actual: 3
        })
    );
}
