#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Error {
    WrongGeneration { expected: usize, actual: usize },
    ElementNotFound,
}
