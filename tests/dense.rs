use gvec::{dvec, replace_expr};
use gvec::{DenseVec, Error, GenerationalIndex};

type GV = DenseVec<u8>;

#[test]
fn test_insertion() {
    let mut temp = GV::with_capacity(5);
    let i = temp.insert(2);
    assert_eq!(temp[i], 2);
    assert_eq!(i.index(), 0);
    assert_eq!(i.generation(), 0);
}

#[test]
#[should_panic]
fn test_removal() {
    let mut temp = GV::with_capacity(5);
    let i = temp.insert(2);
    assert_eq!(temp[i], 2);
    temp.remove(i).unwrap();
    assert_ne!(temp[i], 2);
}

#[test]
fn test_get() {
    let mut temp = GV::with_capacity(5);
    let i = temp.insert(1);
    let value = temp.get(i);
    assert_eq!(value, Some(&1));
}

#[test]
fn test_removal_with_wrong_generation() {
    let mut temp = GV::with_capacity(5);
    let i = temp.insert(2);
    assert_eq!(i.index(), 0);
    assert_eq!(i.generation(), 0);
    let i_2 = GenerationalIndex::new(i.index(), 1);
    assert_eq!(
        temp.remove(i_2),
        Err(Error::WrongGeneration {
            expected: 0,
            actual: 1
        })
    );
}

#[test]
fn test_gvec_macro() {
    let (test, test_indices) = dvec![42, 69, 123];
    assert_eq!(test[test_indices[1]], 69);
    assert_ne!(test[test_indices[2]], 321);
}

#[test]
fn test_gvec_macro_specify_quantity() {
    let (test, test_indices) = dvec![25; "a"];
    assert_eq!(test[test_indices[0]], "a");
    assert_ne!(test[test_indices[1]], "b");
}

#[test]
fn test_insertion_with_reallocation() {
    let (mut test, _) = dvec![1, 2, 3];
    let fourth = test.insert(4);
    assert_eq!(test[fourth], 4);
    assert_ne!(test[fourth], 5);
}
